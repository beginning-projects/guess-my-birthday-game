from random import randint

name = input("Hi! What is your name?")

for guess_number in range(1,6):
    month_number = randint(1,12)
    year_number = randint(1924, 2004)
    print("Guess", guess_number, " : ", name , "were you", "born on ", month_number, "/", year_number, " ?")
    response = input("yes or no?")
    if guess_number < 5:
        if response == "yes":
            print("I knew it!")
            exit()
        elif response == "no":
            print("Drat! Lemme try again!")
    if guess_number == 5:
        print("I have other things to do. Good bye.")
        exit()
